//
//  LocalizationApp.swift
//  Localization
//
//  Created by Eubank,Cameron on 2/10/23.
//

import SwiftUI

@main
struct LocalizationApp: App {
    var body: some Scene {
        WindowGroup {
            ContentView()
        }
    }
}
